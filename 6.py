def sum_of_squares(n):
    # geometric solution: imagine all the squares put next to each other in one line
    # full_square is the length of this line times the height of the highest square
    # then the missing parts get subtracted
    sum_n = n * (n + 1) / 2
    full_square = n * sum_n
    for i in (reversed(range(1, n+1))):
        # i is the length of the square and (n-i) the difference in height compared to the biggest square
        full_square -= (i * (n - i))
    return full_square


def square_of_sum(n):
    sum_n = n * (n + 1) / 2
    return sum_n**2


def sum_sq(n):
    # general formula as suggested in the explaining paper
    return (2 * n + 1) * (n + 1) * n / 6


n = 100
print(square_of_sum(n) - sum_of_squares(n))
print(square_of_sum(n) - sum_sq(n))
