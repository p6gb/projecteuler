import numpy as np

def is_palindrome(n):
    n = str(n)
    return n == n[::-1]   # Inverts String/Array (Extended Slice Syntax), reversed() also works

palindromes = []
for i in range(1, 1000):
    for j in range(1, 1000):
        if i >= j:
            n = i * j
            if is_palindrome(n):
                palindromes.append(n)

print(palindromes)
print(np.max(palindromes))

