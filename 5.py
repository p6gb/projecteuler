import numpy as np

# the number we look for must at least include all prime factors smaller than the number itself,
# we can therefore increment by the product of this set

def find_smallest_multiple(n):
    primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]  # prime numbers < 100
    primes = [p for p in primes if p <= n]
    prime_prod = np.prod(primes)
    current_n = 0
    while True: 
        current_n += prime_prod
        for div in reversed(range(2, n)):
            if current_n % div != 0:
                break
        else:
            return current_n    # this is only reached when disible through all numbers <= n

print(find_smallest_multiple(20))

