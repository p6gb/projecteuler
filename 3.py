import numpy as np

def find_smallest_divisor(n):
    for div in range(2, n):
        if n % div == 0:
            return find_smallest_divisor(int(n/div))
    print(n)

find_smallest_divisor(13195)
find_smallest_divisor(600851475143)

